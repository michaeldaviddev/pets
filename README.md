# Pets

Red Social para compartir imágenes de mascotas

## Para este proyecto se uso:

- Angular
- Angular Material
- [Dummy Api](https://dummyapi.io/)

## Ejecutar proyecto

- Clonar este repositorio
- `npm install` para instalar dependencias
- `ng serve` para ejecutar el proyecto

## Demo

[Ver demo](https://sad-ptolemy-0f186c.netlify.app/)
