export interface User {
  email: string;
  firstName: string;
  lastName: string;
  phone: string;
  picture: string;
}
