import {Owner} from './owner';

export interface Post {
  id: string;
  owner: Owner;
  image: string;
  tags: string[];
  text: string;
  publishDate: string;
  link: string;
  likes: number;
  comments: Comment[];
  numComments: number;
}
