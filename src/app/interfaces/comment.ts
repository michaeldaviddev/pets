import {Owner} from "./owner";

export interface Comment {
  id: string;
  message: string;
  owner: Owner;
  publishDate: string;
}
