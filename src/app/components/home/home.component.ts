import { Component, OnInit } from '@angular/core';
import { DummyApiService } from '../../services/dummy-api.service';
import { Post } from '../../interfaces/post';
import {MatDialog} from "@angular/material/dialog";
import {OwnerDialogComponent} from "../owner-dialog/owner-dialog.component";
import {CommentsDialogComponent} from "../comments-dialog/comments-dialog.component";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  posts: Post[];
  selectedTag: string;
  loading: boolean;

  constructor(private dummyApiService: DummyApiService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getPosts();
  }

  getPosts(): void {
    this.loading = true;
    this.dummyApiService.getPosts().then((r: any) => {
      // console.log(r);
      this.posts = r.data;
      this.selectedTag = '';
    }).then(() => {
      this.posts.forEach((post) => {
        this.dummyApiService.getComments(post.id).then((r: any) => {
          // console.log(r);
          post.comments = r.data;
          post.numComments = r.data.length;
        });
      });
    }).finally(() => {
      this.loading = false;
    });
  }

  openUserDialog(userId) {
    const dialogRef = this.dialog.open(OwnerDialogComponent, {
      data: { id: userId }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  openCommentsDialog(post) {
    const dialogRef = this.dialog.open(CommentsDialogComponent, {
      data: { comments: post.comments }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  getPostsByTag(tagTitle) {
    this.dummyApiService.getPostsByTag(tagTitle).then((r: any) => {
      this.posts = r.data;
      this.selectedTag = tagTitle;
    }).then(() => {
      this.posts.forEach((post) => {
        this.dummyApiService.getComments(post.id).then((r: any) => {
          post.comments = r.data;
          post.numComments = r.data.length;
        });
      });
    }).finally(() => {
      this.loading = false;
    });
  }
}
