import {Component, Inject, OnInit} from '@angular/core';
import {DummyApiService} from "../../services/dummy-api.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Comment} from "../../interfaces/comment";

@Component({
  selector: 'app-comments-dialog',
  templateUrl: './comments-dialog.component.html',
  styleUrls: ['./comments-dialog.component.scss']
})
export class CommentsDialogComponent implements OnInit {
  comments: Comment[];

  constructor(
    public dialogRef: MatDialogRef<CommentsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dummyApiService: DummyApiService
  ) { }

  ngOnInit(): void {
    // this.getComments();
    this.comments = this.data.comments;
  }

  /*getComments(): void {
    this.dummyApiService.getComments(this.data.id).then((r: any) => {
      // console.log(r);
      this.comments = r.data;
    });
  }*/
}
