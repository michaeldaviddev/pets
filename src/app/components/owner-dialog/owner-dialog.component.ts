import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {DummyApiService} from "../../services/dummy-api.service";
import {User} from "../../interfaces/user";

@Component({
  selector: 'app-owner-dialog',
  templateUrl: './owner-dialog.component.html',
  styleUrls: ['./owner-dialog.component.scss']
})
export class OwnerDialogComponent implements OnInit {

  user: User;

  constructor(
    public dialogRef: MatDialogRef<OwnerDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dummyApiService: DummyApiService
  ) { }

  ngOnInit(): void {
    this.getUser();
  }

  getUser(): void {
    this.dummyApiService.getUser(this.data.id).then((r: any) => {
      this.user = r;
    });
  }
}
