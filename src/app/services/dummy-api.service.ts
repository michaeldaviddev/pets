import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DummyApiService {
  private dummyApi = 'https://dummyapi.io/data/api';
  private token = '5f4fb7121543e5852a507db1';

  constructor(private httpClient: HttpClient) { }

  getPosts() {
    const headers = new HttpHeaders()
      .set('app-id', this.token);

    return this.httpClient.get(`${this.dummyApi}/post`, { headers }).toPromise();
  }

  getUser(userId) {
    const headers = new HttpHeaders()
      .set('app-id', this.token);

    return this.httpClient.get(`${this.dummyApi}/user/${userId}`, { headers }).toPromise();
  }

  getComments(postId) {
    const headers = new HttpHeaders()
      .set('app-id', this.token);

    return this.httpClient.get(`${this.dummyApi}/post/${postId}/comment`, { headers }).toPromise();
  }

  getPostsByTag(tagTitle) {
    const headers = new HttpHeaders()
      .set('app-id', this.token);

    return this.httpClient.get(`${this.dummyApi}/tag/${tagTitle}/post`, { headers }).toPromise();
  }
}
